﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectOrienteret
{
    class Program
    {
        static void Main(string[] args)
        {
            Washer GeefyWash = new Washer();
            GeefyWash.Power();
            Console.WriteLine( GeefyWash.StartWash(180, 800, 60));
            GeefyWash.Washing();
            Console.WriteLine( GeefyWash.EndWash());
            Console.ReadKey();
        }
    }
}
